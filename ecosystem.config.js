module.exports = {
  apps: [{
    name: "ITA-Server",
    script: 'npm run start:dev',
    watch: '.',
    exec_mode: "cluster",
    instances: "2",
    log_date_format: 'YYYY-MM-DD HH:mm Z',

    env_development: {
      NODE_ENV: "development",
      test: true,
      JWT_SECRET: "test",
      JWT_PUBLIC: "test",
      JWT_EXPIRES_IN: "365d",
      COOKIE_SECRET: "COOKIE_SECRET",
      ELASTIC_HOST: '10.202.8.2:9200',
      ELASTIC_HOST: 'tic.razhman.net:9200',
      ELASTIC_USERNAME: 'map',
      ELASTIC_PASSWORD: 'Razhman123',
      API_URI: 'http://172.26.138.178',
      API_PORT: 3060
    }
  }]
};
