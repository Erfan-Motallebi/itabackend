import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { EnvService } from '@tayeh/env';

import cookieParser from 'cookie-parser';

import { AppModule } from './app.module';

async function bootstrap() {
  process.env.TZ = 'Asia/Tehran';
  const app = await NestFactory.create(AppModule, {});
  const env = app.get<EnvService>(EnvService);
  app.enableCors({ origin: '*' });
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser(env.get('COOKIE_SECRET')));
  await app.listen(env.get('API_PORT') || 3060);
}
bootstrap();
