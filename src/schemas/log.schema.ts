import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import {  IsString } from 'class-validator';

import { Document } from 'mongoose';

export type LogDocument = Log & Document;

@Schema({
  timestamps: true,
})
export class Log {
  @Prop()
  @IsString()
  username: string;

  @Prop()
  @IsString()
  module: string;

  @Prop()
  @IsString()
  url: string;
  
  @Prop()
  @IsString()
  message: string;

  @Prop() createdAt?: Date;
}

export const LogSchema = SchemaFactory.createForClass(Log);