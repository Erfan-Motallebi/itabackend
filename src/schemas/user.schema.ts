import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsOptional, IsString } from 'class-validator';

import { Document } from 'mongoose';

import { AccessRoles, excludedWidgetsEnum } from './../internal-auth/hasAccess';
import { Filter } from '../user-control/dto/user-control.dto';

export type UserDocument = User & Document;

@Schema({
  timestamps: true,
})
export class User {
  @Prop()
  username: string;

  @Prop()
  password: string;

  @Prop({ type: [String], enum: AccessRoles, default: AccessRoles.General })
  roles: AccessRoles[];

  @Prop({ type: Filter, default: { ases: [], ips: [], isps: [] } })
  filters: Filter;

  @Prop({ type: [Number], enum: excludedWidgetsEnum })
  excludedWidgets: excludedWidgetsEnum[];

  @Prop()
  first_name: string;

  @Prop()
  last_name: string;

  @Prop()
  position: string;

  @Prop({ default: '' })
  @IsString()
  @IsOptional()
  image?: string;

  @Prop()
  changePass: boolean;

  @Prop() deletedAt?: Date;
  @Prop() createdAt?: Date;
  @Prop() updatedAt?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
