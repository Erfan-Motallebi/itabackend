import { Type } from '@nestjs/common';

import { EnvModule } from '@tayeh/env';

import { Cache } from 'cache-manager';

export const envModule = EnvModule.register({
  path:
    __dirname +
    `/../../.env/${typeof process.env.NODE_ENV == 'undefined'
      ? 'development'
      : process.env.NODE_ENV
    }.env`,
});


export class AsyncRedisProperty<T> {
  constructor({ cacheManager: cacheManager, key: key, default: defaultValue }) {
    this.key = key
    this.cacheManager = cacheManager
    this.defaultValue = defaultValue || {}
  }
  private key: string;
  private cacheManager: Cache;
  private defaultValue?: T;

  async update(objectToAdd: { [key: string]: T }): Promise<void> {
    let oldData = await this.get()
    this.set({ ...oldData, ...objectToAdd })
  }

  async get(key?: string): Promise<T> {
    let redisValues = (await this.cacheManager.get(this.key))
      ? await this.cacheManager.get(this.key) as any
      : []
    if (Object.keys(redisValues || {}).length < Object.keys(this.defaultValue).length) {
      redisValues = this.defaultValue
      await this.setDefault()
    }
    if (key && redisValues) return redisValues[key]
    else return redisValues
  }

  async setDefault() {
    return await this.set(this.defaultValue || {})
  }

  async set(redisEntry: any, ttl?: number): Promise<Boolean> {
    redisEntry = redisEntry
    await this.cacheManager.set(this.key, redisEntry, ttl = 0)
    return true
  }

  async del<T>(redisKey: string): Promise<Boolean> {
    if (!redisKey) return
    let oldData = await this.get()

    if (oldData[redisKey]) {
      oldData[redisKey] = undefined
      delete oldData[redisKey.toString()]
      await this.set(oldData)
      return true
    }
  }

}