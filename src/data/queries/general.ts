export const asListSearchQuery = {
    "aggs": {
        "srcISPs": {
            "terms": {
                "field": "src_isp.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        },
        "destISPs": {
            "terms": {
                "field": "dest_isp.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        },
        "srciFaces": {
            "terms": {
                "field": "src_iface.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        },
        "destiFaces": {
            "terms": {
                "field": "dest_iface.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        }
    },
    "size": 0,
    "fields": [
        {
            "field": "@timestamp",
            "format": "date_time"
        }
    ],
    "script_fields": {},
    "stored_fields": [
        "*"
    ],
    "runtime_mappings": {},
    "_source": {
        "excludes": []
    },
    "query": {
        "bool": {
            "must": [],
            "filter": [
                {
                    "range": {
                        "@timestamp": {
                            "time_zone": "Asia/Tehran",
                            "gte": "now-10s"
                        }
                    }
                },
                {
                    "match_phrase": {
                        "dest_country": "IR"
                    }
                },
                {
                    "match_phrase": {
                        "log_type": "asample"
                    }
                }
            ],
            "should": [],
            "must_not": [
                {
                    "match_phrase": {
                        "src_country": "IR"
                    }
                }
            ]
        }
    }
}


export const ispListSearchQuery = {
    "aggs": {
        "srcISPs": {
            "terms": {
                "field": "src_isp.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        },
        "destISPs": {
            "terms": {
                "field": "dest_isp.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        },
        "srciFaces": {
            "terms": {
                "field": "src_iface.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        },
        "destiFaces": {
            "terms": {
                "field": "dest_iface.keyword",
                "order": {
                    "_count": "desc"
                },
                "size": 500
            }
        }
    },
    "size": 0,
    "fields": [
        {
            "field": "@timestamp",
            "format": "date_time"
        }
    ],
    "script_fields": {},
    "stored_fields": [
        "*"
    ],
    "runtime_mappings": {},
    "_source": {
        "excludes": []
    },
    "query": {
        "bool": {
            "must": [],
            "filter": [
                {
                    "range": {
                        "@timestamp": {
                            "time_zone": "Asia/Tehran",
                            "gte": "now-10s"
                        }
                    }
                },
                {
                    "match_phrase": {
                        "dest_country": "IR"
                    }
                },
                {
                    "match_phrase": {
                        "log_type": "asample"
                    }
                }
            ],
            "should": [],
            "must_not": [
                {
                    "match_phrase": {
                        "src_country": "IR"
                    }
                }
            ]
        }
    }
}