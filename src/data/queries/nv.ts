export let nvGeneralAggs = {
    "protocols": {
        "terms": {
            "field": "protocol.keyword",
            "order": {
                "_count": "desc"
            },
            "size": 5
        }
    },
    "topZones": {
        "terms": {
            "field": "src_iface.keyword",
            "order": {
                "_count": "desc"
            },
            "size": 5
        }
    },
    "src_isps": {
        "terms": {
            "field": "src_isp.keyword",
            "order": {
                "_count": "desc"
            },
            "size": 5
        }
    },
    "dest_isps": {
        "terms": {
            "field": "dest_isp.keyword",
            "order": {
                "_count": "desc"
            },
            "size": 5
        }
    },
    "src_ases": {
        "terms": {
            "field": "src_as",
            "order": {
                "_count": "desc"
            },
            "size": 5
        }
    },
    "dest_ases": {
        "terms": {
            "field": "dest_as",
            "order": {
                "_count": "desc"
            },
            "size": 5
        }
    },
}
export let nvProtocolAggs = {
    "tcp": {
        "filter": {
            "term": {
                "protocol": "tcp"
            }
        },
        "aggs": {
            "src_ips": {
                "terms": {
                    "field": "src_ip.keyword",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "dest_ips": {
                "terms": {
                    "field": "dest_ip.keyword",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "src_ports": {
                "terms": {
                    "field": "src_port",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "dest_ports": {
                "terms": {
                    "field": "dest_port",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "flagsHolder": {
                "filter": {
                    "bool": {
                        "must_not": [
                            {
                                "match": {
                                    "flags.keyword": ""
                                }
                            }
                        ]
                    }
                },
                "aggs": {
                    "flags": {
                        "terms": {
                            "field": "flags.keyword",
                            "order": {
                                "_count": "desc"
                            },
                            "size": 10
                        }
                    }
                }
            },
            "60t128": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "60",
                                        "lt": "128"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "128t256": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "127",
                                        "lt": "256"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "256t512": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "255",
                                        "lt": "512"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "512t1024": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "511",
                                        "lt": "1024"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "gt1024": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "1023"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            }

        }
    },
    "udp": {
        "filter": {
            "term": {
                "protocol": "udp"
            }
        },
        "aggs": {
            "src_ips": {
                "terms": {
                    "field": "src_ip.keyword",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "dest_ips": {
                "terms": {
                    "field": "dest_ip.keyword",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "src_ports": {
                "terms": {
                    "field": "src_port",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "dest_ports": {
                "terms": {
                    "field": "dest_port",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "60t128": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "60",
                                        "lt": "128"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "128t256": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "127",
                                        "lt": "256"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "256t512": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "255",
                                        "lt": "512"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "512t1024": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "511",
                                        "lt": "1024"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "gt1024": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "1023"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            }

        }
    },
    "icmp": {
        "filter": {
            "term": {
                "protocol": "icmp"
            }
        },
        "aggs": {
            "src_ips": {
                "terms": {
                    "field": "src_ip.keyword",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "dest_ips": {
                "terms": {
                    "field": "dest_ip.keyword",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5
                }
            },
            "60t128": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "60",
                                        "lt": "128"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "128t256": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "127",
                                        "lt": "256"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "256t512": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "255",
                                        "lt": "512"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "512t1024": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "511",
                                        "lt": "1024"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            },
            "gt1024": {
                "filter": {
                    "bool": {
                        "should": [
                            {
                                "range": {
                                    "pkt_size": {
                                        "gt": "1023"
                                    }
                                }
                            }
                        ],
                        "minimum_should_match": 1
                    }
                }
            }

        }
    }
}