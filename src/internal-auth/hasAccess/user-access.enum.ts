export enum AccessRoles {
  All = "all",
  NetworkVisibility = "nv",
  ApplicationVisibility = "av",
  UserControl = "uc",
  General = "gen",
}
export enum excludedWidgetsEnum {
  // Panes
  Countries_Pane,
  // Filters buttons and panel
  Filters_Panel,
  // Filters fields
  Zone_Filter,
  Country_Filter,
  Source_AS_Filter,
  Destination_AS_Filter,
  Source_ISP_Filter,
  Destination_ISP_Filter,
  Protocol_Filter,
  Packet_Size_Filter,
  TCP_Flags_Filter,
  Application_Layer_Filter,
  // Widgets
  Top_Protocols,
  Top_Zones,
  Top_Source_ISP,
  Top_Destination_ISP,
  Top_Source_AS,
  Top_Destination_AS,
  // TCP Widgets
  TCP_Flags,
  TCP_Packet_Size,
  TCP_Source_IP,
  TCP_Destination_IP,
  TCP_Source_Port,
  TCP_Destination_Port,
  // UDP Widgets
  UDP_Packet_Size,
  UDP_Source_IP,
  UDP_Destination_IP,
  UDP_Source_Port,
  UDP_Destination_Port,
  // ICMP Widgets
  ICMP_Packet_Size,
  ICMP_Source_IP,
  ICMP_Destination_IP,
  // Tabs
  TCP_Tab,
  UDP_Tab,
  ICMP_Tab,

  // Application Layer
  Top_Games,
  Top_Services,
  Top_Websites,
}

// This must be the same as excludedWidgetsEnum
// ( this actually helps dynamically removing the excluded
// widgets from requestBody obj in tia.service )
export enum excludedWidgetsInQueryEnum {
  // Panes
  Countries_Pane = "countries",
  // Filters buttons and panel
  Filters_Panel = "",
  // Filters fields
  Zone_Filter = "",
  Country_Filter = "",
  Source_AS_Filter = "",
  Destination_AS_Filter = "",
  Source_ISP_Filter = "",
  Destination_ISP_Filter = "",
  Protocol_Filter = "",
  Packet_Size_Filter = "",
  TCP_Flags_Filter = "",
  Application_Layer_Filter = "",
  // Widgets
  Top_Protocols = "protocols",
  Top_Zones = "topZones",
  Top_Source_ISP = "src_isps",
  Top_Destination_ISP = "dest_isps",
  Top_Source_AS = "src_ases",
  Top_Destination_AS = "dest_ases",
  // TCP Widgets
  TCP_Flags = "tcp.flagsHolder",
  TCP_Packet_Size = "",
  TCP_Source_IP = "tcp.src_ips",
  TCP_Destination_IP = "tcp.dest_ips",
  TCP_Source_Port = "tcp.src_ports",
  TCP_Destination_Port = "tcp.dest_ports",
  // UDP Widgets
  UDP_Packet_Size = "",
  UDP_Source_IP = "udp.src_ips",
  UDP_Destination_IP = "udp.dest_ips",
  UDP_Source_Port = "udp.src_ports",
  UDP_Destination_Port = "udp.dest_ports",
  // ICMP Widgets
  ICMP_Packet_Size = "",
  ICMP_Source_IP = "icmp.src_ips",
  ICMP_Destination_IP = "icmp.dest_ips",
  // Tabs
  TCP_Tab = "tcp",
  UDP_Tab = "udp",
  ICMP_Tab = "icmp",

  // Application Layer
  Top_Games = "src_ports",
  Top_Services = "src_ports",
  Top_Websites = "dest_ips",
}