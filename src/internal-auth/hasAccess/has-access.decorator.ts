import { SetMetadata } from "@nestjs/common";
import { AccessRoles } from "./user-access.enum";

export const HasAccess = (roles: AccessRoles[] | AccessRoles) =>
    SetMetadata(
        'accessRoles',
        typeof roles == 'string' ? [roles] : roles,
    );