import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';

import { Observable } from 'rxjs';

import { Log, LogDocument } from './../../schemas/log.schema';

import { Model } from 'mongoose';

import { AccessRoles } from './user-access.enum';

@Injectable()
export class HasAccessGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        @InjectModel(Log.name) private readonly logger: Model<LogDocument>,
    ) { }
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        const user = request.user;
        //Priority on method meta
        let roles = this.reflector.get<AccessRoles[]>(
            'accessRoles',
            context.getHandler(),
        );
        if (!roles)
            //Check for class meta
            roles = this.reflector.get<AccessRoles[]>(
                'accessRoles',
                context.getClass(),
            );
        let action = user?.role?.includes(AccessRoles.All) ? true : roles.every(v => user?.role?.includes(v));
        return action
    }
}
