import { BadRequestException, Inject, Injectable, Scope, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { REQUEST } from '@nestjs/core';

import { AuthorizedRequest, AuthService } from '@tayeh/auth';

import mongoose, { Model } from 'mongoose';

import * as bcrypt from 'bcryptjs';

import { ChangePassRequest } from '../dto/request/changePass.req';
import { Log, LogDocument } from '../schemas/log.schema';

import { LoginRequest } from '../dto/request';
import { User, UserDocument } from '../schemas/user.schema';

import { AccessRoles } from './hasAccess';

@Injectable()
export class InternalAuthService {
  constructor(
    @InjectModel(User.name) private readonly user: Model<UserDocument>,
    @InjectModel(Log.name) private readonly logger: Model<LogDocument>,
    private auth: AuthService,
  ) { }
  async onApplicationBootstrap() {
    const count = await this.user.count({}).exec();
    console.log({ count });
    if (count == 0) {
      const users: User[] = [
        {
          username: 'ita',
          first_name: 'Admin',
          last_name: '',
          position: 'Administrator',
          roles: [AccessRoles.UserControl, AccessRoles.NetworkVisibility],
          filters: { ases: [], isps: [], ips: [] },
          image: '',
          excludedWidgets: [],
          changePass: false,
          password: await bcrypt.hash('@Razhman@123', 10),
        },
      ];
      this.user.insertMany(users);
    }
  }
  fetchUser = async (requestedUser: AuthorizedRequest) => {
    const user = await this.user.findOne({ _id: requestedUser.user.id }).exec();
    const token = await this.auth.sign(
      {
        sub: user?._id,
        iss: 'razhman',
        r: user.roles,
        i: user.username,
      },
      { expiresIn: '24h' },
    );
    return {
      id: user._id,
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name,
      roles: user.roles,
      filters: user.filters,
      image: user.image,
      excludedWidgets: user.excludedWidgets,
      position: user.position,
      changePass: user.changePass,
    };
  };
  async login(body: LoginRequest) {
    try {
      const user = await this.user.findOne({ username: body.username }).exec();
      if (await bcrypt.compare(body.password, user.password)) {
        const token = await this.auth.sign(
          {
            sub: user._id,
            iss: 'razhman',
            r: user.roles,
            i: user.username
          },
          { expiresIn: '24h' },
        );
        this.logger.create({ url: '', module: 'AUTH|LOGIN', username: user.username, message: 'ACCEPTED' })
        return {
          token,
          id: user._id,
          roles: user.roles,
          user: {
            id: user._id,
            username: user.username,
            first_name: user.first_name,
            last_name: user.last_name,
            roles: user.roles,
            filters: user.filters,
            image: user.image,
            excludedWidgets: user.excludedWidgets,
            position: user.position,
            changePass: user.changePass,
          },
        };
      } else {
        this.logger.create({ url: '', module: 'AUTH|LOGIN', username: user.username, message: 'DENIED => Password is not correct' })
        throw new UnauthorizedException({
          errors: ['پسورد وارد شده صحیح نمی باشد'],
        });
      }
    } catch (err) {
      console.log(err);
      throw new UnauthorizedException({
        errors: ['پسورد وارد شده صحیح نمی باشد'],
      });
    }
  }

  async changePass(userId: mongoose.Types.ObjectId, req: ChangePassRequest) {
    let user = await this.user.findOne({ _id: userId }).exec()
    if (!user) {
      this.logger.create({ url: '', module: 'AUTH|CHANGEPASS', username: user.username, message: `DENIED => User not found(${userId})` })
      throw new UnauthorizedException({ errors: ['User was not found'] })
    }
    if (req.newPassword != req.newPasswordConfirm) {
      this.logger.create({ url: '', module: 'AUTH|CHANGEPASS', username: user.username, message: `DENIED => Passwords do not match(${req.newPassword}) and(${req.newPasswordConfirm})` })
      throw new UnauthorizedException({ errors: ['New passwords do not match'] })
    }
    if (!await bcrypt.compare(req.currentPassword, user.password)) {
      this.logger.create({ url: '', module: 'AUTH|CHANGEPASS', username: user.username, message: `DENIED => Current Password(${req.currentPassword}) is not correct for (${user.username})` })
      throw new UnauthorizedException({ errors: ['Current Password is not correct'] })
    }
    user.password = await bcrypt.hash(req.newPassword, 10)
    user.changePass = false
    await user.save()
    let token = await this.auth.sign(
      {
        sub: user._id,
        iss: 'razhman',
        r: user.roles,
      },
      { expiresIn: '24h' },
    )
    this.logger.create({ url: '', module: 'AUTH|CHANGEPASS', username: user.username, message: `ACCEPTED => Changed password from(${req.currentPassword}) to(${req.newPassword})` })
    return {
      token,
      id: user._id,
      roles: user.roles,
      user: {
        id: user._id,
        username: user.username,
        first_name: user.first_name,
        last_name: user.last_name,
        roles: user.roles,
        filters: user.filters,
        image: user.image,
        excludedWidgets: user.excludedWidgets,
        position: user.position,
        changePass: user.changePass,
      },
    }
  }
}
