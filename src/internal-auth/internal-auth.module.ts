import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { Log, LogSchema } from 'src/schemas/log.schema';

import { User, UserSchema } from '../schemas/user.schema';

import { InternalAuthController } from './internal-auth.controller';
import { InternalAuthService } from './internal-auth.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Log.name, schema: LogSchema },
    ]),
  ],
  controllers: [InternalAuthController],
  providers: [InternalAuthService],
})
export class InternalAuthModule {}
