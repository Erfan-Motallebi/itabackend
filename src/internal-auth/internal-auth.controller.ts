import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';

import { AuthorizedRequest, JwtAuthGuard } from '@tayeh/auth';

import { Response } from 'express';

import { ChangePassRequest } from 'src/dto/request/changePass.req';

import mongoose from 'mongoose';

import { LoginRequest } from '../dto/request';

import { InternalAuthService } from './internal-auth.service';

@Controller('auth')
export class InternalAuthController {
  constructor(private readonly service: InternalAuthService) { }

  @UseGuards(JwtAuthGuard)
  @Get('/')
  async auth(@Req() req: AuthorizedRequest) {
    try {
      return this.service.fetchUser(req);
    } catch (err) {
      throw err;
    }
  }

  @Post('/login')
  async login(
    @Body() body: LoginRequest,
    @Res({ passthrough: true }) res: Response,
  ) {
    try {
      const data = await this.service.login(body);
      res.cookie('user', data.token, {
        maxAge: 31536000000,
        sameSite: 'none',
        domain: 'razhman.com',
        signed: true,
        secure: false,
        httpOnly: true,
      });
      return data;
    } catch (err) {
      throw err;
    }
  }

  @Put(':id/changePass')
  async changePass(
    @Body() req: ChangePassRequest,
    @Param('id') id: mongoose.Types.ObjectId
  ) {
    try {
      return this.service.changePass(id, req)
    } catch (err) {
      throw err;
    }
  }
}
