import { Interval } from '@nestjs/schedule';
import { BadRequestException, Inject, Injectable, forwardRef } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { CACHE_MANAGER } from '@nestjs/cache-manager';

import { EnvService } from '@tayeh/env';

import { Cache } from 'cache-manager';

import { type Socket } from 'socket.io';

import { firstValueFrom } from 'rxjs';

import { User } from '../schemas/user.schema';
import { NotificationGateway } from '../notification/notification.gateway';
import { excludedWidgetsInQueryEnum } from '../internal-auth/hasAccess';
import { nvGeneralAggs, nvProtocolAggs } from '../data/queries/nv';

import FormData from 'form-data';

import { AsyncRedisProperty } from '../_shared/env';
import { asListSearchQuery, ispListSearchQuery } from '../data/queries/general';

const https = require('https');

const env = new EnvService('./.env/development.env')

@Injectable()
export class ITAService {
    constructor(
        @Inject(forwardRef(() => NotificationGateway)) private notification: NotificationGateway,
        private httpService: HttpService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache
    ) { }

    /**
    ** @summary data for the Source ISPs changing with an interval of 10s
    */

    private srcISPs = new AsyncRedisProperty<any[]>({
        cacheManager: this.cacheManager,
        key: 'srcISPs',
        default: [],
    });

    /**
     ** @summary data for the Destination ISPs changing with an interval of 10s
     */
    private destISPs = new AsyncRedisProperty<any[]>({
        cacheManager: this.cacheManager,
        key: 'destISPs',
        default: [],
    });

    /**
     ** @summary data for the Source Interfaces changing with an interval of 10s
     */
    private srciFaces = new AsyncRedisProperty<any[]>({
        cacheManager: this.cacheManager,
        key: 'srciFaces',
        default: [],
    });

    /**
     ** @summary data for the Destination Interfaces changing with an interval of 10s
     */
    private destiFaces = new AsyncRedisProperty<any[]>({
        cacheManager: this.cacheManager,
        key: 'destiFaces',
        default: [],
    });

    /**
     ** @summary Container for the users's filter panel
     */
    private filters = new AsyncRedisProperty<any[]>({
        cacheManager: this.cacheManager,
        key: 'filters',
        default: { nvgeneral: [], avgeneral: [] },
    });

    /**
     ** @summary Container for users' map data filled by the request through their Socket ID
     */
    private mapData = new AsyncRedisProperty<any[]>({
        cacheManager: this.cacheManager,
        key: 'mapData',
        default: { nvgeneral: [], avgeneral: [] },
    });

    /**
     ** @summary Container for users's data type [ av or nv ] through their Socket ID
     */
    private dataTypes = new AsyncRedisProperty<string>({
        cacheManager: this.cacheManager,
        key: 'dataTypes',
        default: { nvgeneral: 'nv', avgeneral: 'av' },
    });

    async onApplicationBootstrap() {
        console.log(process.env.NODE_APP_INSTANCE);
        if (
            process.env.NODE_APP_INSTANCE != '0' &&
            process.env.NODE_APP_INSTANCE != undefined
        )
            return;

        /**
         * @summary Fill the default for users's mapData/dataTypes/filters not to face an error in [Redis]
         */
        this.mapData.setDefault();
        this.dataTypes.setDefault();
        this.filters.setDefault();

        /**
         * @summary Scheduler's methods must be called in onApplicationBootstrap lifecycle
         */
        this.getISPList();
        this.renewMapData();
    }

    /**
  * @function getISPList
  * @summary Refreshing list of ISPs and interfaces with the new ones for the filter fields
  * @returns
  */

    @Interval(10000)
    async getISPList() {
        if (process.env.NODE_APP_INSTANCE != '0' && process.env.NODE_APP_INSTANCE != undefined) return

        try {
            let { aggregations } = (await firstValueFrom(this.httpService.get(
                `https://${env.get('ELASTIC_USERNAME')}:${env.get('ELASTIC_PASSWORD')}@${env.get('ELASTIC_HOST')}/log_pps_rule-detector*/_search`,
                {
                    data: ispListSearchQuery,
                    httpsAgent: new https.Agent({
                        rejectUnauthorized: false
                    })
                }
            )
            )).data
            //! Setting the data to our memory variables [ Redis ]
            const srcISPsSetting = this.srcISPs.set(aggregations.srcISPs?.buckets?.map((r: any) => r.key))
            const destISPsSetting = this.destISPs.set(aggregations.destISPs?.buckets?.map((r: any) => r.key))
            const srciFacesSetting = this.srciFaces.set(aggregations.srciFaces?.buckets?.map((r: any) => r.key))
            const destiFacesSetting = this.destiFaces.set(aggregations.destiFaces?.buckets?.map((r: any) => r.key))
            await Promise.allSettled([srcISPsSetting, destISPsSetting, srciFacesSetting, destiFacesSetting])
        } catch (error) {
            console.log(error)
        }

    }

    /**
  * @function renewMapData
  * @summary Checks the client map data for new data every 15 seconds [ Interval Mode ] for all users with their specific filters and network access limit
  * @returns
  */

    @Interval(15000)
    async renewMapData() {
        if (process.env.NODE_APP_INSTANCE != '0' && process.env.NODE_APP_INSTANCE != undefined) return
        //! looping through the map data to freshen the data inside

        let mapDataObj: any = await this.mapData.get()
        // console.log({ reNewMapData: mapDataObj });
        // console.log({ renewMapDataLength: Object.keys(mapDataObj).length });
        // if (Object.keys(mapDataObj).length < 3) {
        //     console.log({ mapDataKeys: 'Less than 3' });
        //     await this.mapData.setDefault()
        //     await this.dataTypes.setDefault()
        //     mapDataObj = { nvgeneral: [], avgeneral: [] }
        // }
        for (const key in mapDataObj) {
            if (Object.prototype.hasOwnProperty.call(await this.mapData.get(), key)) {
                const userFilters = await this.filters.get(key);
                let user = !key.includes('general') ? await this.notification.getUserFromSocketID(key) : null
                if (!Object.prototype.hasOwnProperty.call(await this.mapData.get(), key)) return // if data was deleted in the meanwhile we're in loop
                this.getDataFromElastic(key, userFilters, user)
            }
        }
    }


    // NOTE: clearing filters is also done with this approach
    // TODO: Check the users' filters with the old one

    /**
  * @function setUserFilter
  * @summary It handles the user's filter through the filter panel whether it's empty or filled
  * @param clientSocket Socket recieved from the client
  * @param filters filters received from the user filter panel
  * @return
  */

    async setUserFilter(clientSocket: Socket, filters: any) {
        let filtersTemp = [];

        for (const [key, value] of Object.entries(filters)) {
            const isFilterKeyExist = [
                'src_country',
                'src_as',
                'dest_as',
                'src_isp',
                'dest_isp',
                'src_ip',
                'dest_ip',
                'src_port',
                'dest_port',
                'src_iface',
                'dest_iface',
                'protocol',
                'flags',
                'pkt_size',
                'raw_filter',
            ].includes(key);
            if (isFilterKeyExist) filtersTemp.push({ [key]: value });
        }
        //! Checking the items in the array to see if there're any filters or not (Empty filters mean clearing the filter)
        if (filtersTemp.length > 0) {
            //! deleting the mapdata
            const mapDataDeleting = this.mapData.del(clientSocket.id)
            //! Replacing the old filters with the new one the user
            const filterUpdating = this.filters.update({ [clientSocket.id]: filtersTemp });
            await Promise.allSettled([mapDataDeleting, filterUpdating])
            //! Requesting for fresh data with true dreshness flag
            await this.getLiveData(clientSocket, filtersTemp, true);
        } else {
            //! deleting mapData and filters so
            const mapDataDEleting = this.mapData.del(clientSocket.id)
            const filterDeleting = this.filters.del(clientSocket.id)
            await Promise.allSettled([mapDataDEleting, filterDeleting])
            //! no filtered data will be requested
            await this.getLiveData(clientSocket, [], true);
        }
    }



    async getDestISPList() {
        // returning our lates destISP list
        return await this.destISPs.get();
    }

    async getASList() {
        // requesting for AS lists, it will be used
        // in user settings for the AS limit fields

        try {
            let { aggregations } = (await firstValueFrom(this.httpService.get(
                `https://${env.get('ELASTIC_USERNAME')}:${env.get('ELASTIC_PASSWORD')}@${env.get('ELASTIC_HOST')}/log_pps_rule-detector*/_search`,
                {
                    data: asListSearchQuery,
                    httpsAgent: new https.Agent({
                        rejectUnauthorized: false
                    })
                }
            ))).data
            return aggregations.destASes?.buckets.map((r: any) => r.key);
        } catch (error) {
            console.log(error)
            return []
        }

    }

    //! Clearing user data so user will get real data when requesting
    async flushUserData(socketID: string) {
        await this.mapData.del(socketID)
        const userSocket = this.notification.server.sockets.sockets?.get(socketID)
        this.getLiveData(userSocket, await this.filters.get(socketID), true)
    }

    async sendDataToClient(clientSocket: Socket, data) {
        //! sending the map data to clients through emitting an event from the client-side
        clientSocket.emit('mapData', { ...data, ...{ srcISPs: await this.srcISPs.get(), destISPs: await this.destISPs.get(), srciFaces: await this.srciFaces.get(), destiFaces: await this.destiFaces.get() } })
    }

    async handleUserDisconnect(client: Socket) {
        //! Removing unnecessary fields from the client map data and filters so as to faciliate the renewMapData method
        console.log('Handling user disconnetion');
        await this.mapData.del(client.id)
        await this.filters.del(client.id)
    }
    async setDataType(clientSocket: Socket, type: 'nv' | 'av') {
        let oldType = await this.dataTypes.get(clientSocket.id)
        // new dataType will be set
        this.dataTypes.update({ [clientSocket.id]: type })
        if (type != oldType) {
            // deleting the mapdata and filters because the client has changed
            // the data type so we will need fresh data for the client
            await this.mapData.del(clientSocket.id)
            await this.filters.del(clientSocket.id)
            const user: User = await this.notification.getUserFromSocket(clientSocket)
            let freshData = true
            if (!user?.filters.ases.length && !user?.filters.isps.length && !user?.filters.ips.length)
                freshData = false
            this.getLiveData(clientSocket, await this.filters.get(clientSocket.id), freshData)
        } else {
            this.getLiveData(clientSocket, await this.filters.get(clientSocket.id))
        }
    }

    /**
     * @function getLiveData
     * @param clientSocket Socket recieved from the client
     * @param filters Filters received from the user filter panel
     * @param dataFreshReq A boolean to check data freshness
     * @returns 
     */

    async getLiveData(clientSocket: Socket, filters = [], dataFreshReq = false) {
        if (!clientSocket) return

        // * Getting the user requesting for map data
        let user = await this.notification.getUserFromSocket(clientSocket)
        // * Checking the old client map data
        let clientMapdata = await this.mapData.get(clientSocket.id)

        // * Checking user network access limit
        const HasUserAsesAccessLimit = user?.filters?.ases?.length > 0
        const HasUserIspsAccessLimit = user?.filters?.isps?.length > 0
        const HasUserIpsAccessLimit = user?.filters?.ips?.length > 0

        if (user && (HasUserAsesAccessLimit || HasUserIspsAccessLimit || HasUserIpsAccessLimit)) {
            if (clientMapdata && !dataFreshReq) {
                // * map data already exists OR no need freshening map data
                console.log('mapdata exists')
                this.sendDataToClient(clientSocket, clientMapdata)
            } else {
                // * map data not exist OR freshening map data
                console.log('fresh data')
                let data = await this.getDataFromElastic(clientSocket.id, filters, user)
                this.sendDataToClient(clientSocket, data)
            }
        }
        else if (user) {
            // * Users with general data [ no limits, so all data ]
            if (clientMapdata && !dataFreshReq) {
                // * map data already exists OR no need freshening map data
                this.sendDataToClient(clientSocket, clientMapdata)
            } else if (dataFreshReq) {
                // * Map data exists but requesting for a fresh data
                let data = await this.getDataFromElastic(clientSocket.id, filters, user)
                this.sendDataToClient(clientSocket, data)
            } else {
                //* no limits and filters, general sent
                console.log('sending general')
                this.sendDataToClient(clientSocket, await this.mapData.get(`${await this.dataTypes.get(clientSocket?.id)}general`))
            }

        }
    }


    async getDataFromElasticReq(type, username) {
        const user = await this.notification.getUserModel(username)
        if (!user) throw new BadRequestException('Wrong Username requested', null)
        await this.dataTypes.update({ [`${type}temp`]: type })
        return await this.getDataFromElastic(`${type}temp`, [], user)
    }

    /**
     * @function getDataFromElastic
     * @summary It basically sends queries to ES with the given filters and users limit
     * @param key client socket id to get its proper info from redis
     * @param filters filters being recieved by the client [ empty filters || filled filters ]
     * @param user User requesting the new data from elasticSearch
     * @returns new map data after updating the map data inside Redis
     */
    async getDataFromElastic(key: string, filters = [], user: User = undefined) {

        const type = await this.dataTypes.get(key)
        const allFilters = await this.filters.get()
        if (!Object.prototype.hasOwnProperty.call(allFilters, key)) {
            console.log('Filter key not found');
            await this.mapData.del(key)
        }
        let requestBody: any = {
            "aggs": {
                "countries": {
                    "terms": {
                        "field": "src_country.keyword",
                        "order": {
                            "_count": "desc"
                        },
                        "size": 5000
                    }
                }
            },
            "size": 0,
            "fields": [
                {
                    "field": "@timestamp",
                    "format": "date_time"
                }
            ],
            "script_fields": {},
            "stored_fields": [
                "*"
            ],
            "runtime_mappings": {},
            "_source": {
                "excludes": []
            },
            "query": {
                "bool": {
                    "must": [],
                    "filter": [
                        {
                            "range": {
                                "@timestamp": {
                                    "time_zone": "Asia/Tehran",
                                    "gte": "now-15s"
                                }
                            }
                        },
                        ...filters
                            ?.filter(filterObj => !Object.keys(filterObj)[0].includes('_port') && Object.keys(filterObj)[0] != 'raw_filter')
                            .map((filter) => {
                                // looping through filters and making a elastic type of filter query
                                let key = Object.keys(filter)[0]
                                let value = Object.values(filter)[0]
                                let esMatchFilterObj = { "match": {} }
                                if (key === 'protocol') {
                                    esMatchFilterObj['match'][`${key}${(!key.includes('as') && !key.includes('port')) ? '' : ''}`] = value
                                } else {

                                    esMatchFilterObj['match'][`${key}${(!key.includes('as') && !key.includes('port')) ? '.keyword' : ''}`] = value
                                }
                                return esMatchFilterObj
                            }),
                        (filters?.find(f => Object.keys(f)[0] == 'src_port') && filters?.find(f => Object.keys(f)[0] == 'dest_port')) &&
                        {
                            // if source port or dest port is needed in query, we filter them like this as a should query
                            "bool": {
                                "should": [
                                    {
                                        "match": {
                                            "src_port": filters.find(f => Object.keys(f)[0] == 'src_port').src_port
                                        }
                                    },
                                    {
                                        "match": {
                                            "dest_port": filters.find(f => Object.keys(f)[0] == 'dest_port').dest_port
                                        }
                                    }
                                ],
                                "minimum_should_match": 1
                            }
                        },
                        {
                            "match_phrase": {
                                "dest_country": "IR"
                            }
                        },
                        (filters?.find(f => Object.keys(f)[0] == 'raw_filter')) && filters?.find(f => Object.keys(f)[0] == 'raw_filter').raw_filter, // if raw filter is set, just put it here
                        {
                            "match_phrase": {
                                "log_type": "asample"
                            }
                        }
                    ],
                    "should": [],
                    "must_not": [
                        {
                            "match_phrase": {
                                "src_country": "IR"
                            }
                        }
                    ]
                }
            }
        }
        if (type == 'nv') {
            // if requested data type is network visiblity, we set the essential aggrigations
            requestBody.aggs = { ...requestBody.aggs, ...nvGeneralAggs, ...nvProtocolAggs }
        }
        if (type == 'av') {
            // if requested data type is application visiblity, we set the essential aggrigations
            requestBody.aggs.dest_ips = {
                "terms": {
                    "field": "dest_ip.keyword",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 1000
                }
            }
            requestBody.aggs.src_ports = {
                "terms": {
                    "field": "src_port",
                    "order": {
                        "_count": "desc"
                    },
                    "size": 5000
                }
            }
        }
        if (user) {
            // here is for user limits ( set in user profile by admin )
            let filterObj;
            // User AS LIMITS
            if (user.filters?.ases?.length > 0) {
                filterObj = {
                    "bool": {
                        "should": [
                        ],
                        "minimum_should_match": 1
                    }
                };

                // set as filters
                user.filters?.ases.forEach((as: number) => {
                    filterObj.bool.should.push({
                        "match": {
                            "dest_as": as
                        }
                    })
                });
                // add the object to query
                requestBody.query.bool.filter.push(filterObj)
            }
            // User ISP LIMITS
            if (user.filters?.isps?.length > 0) {
                filterObj = {
                    "bool": {
                        "should": [
                        ],
                        "minimum_should_match": 1
                    }
                };
                // set isp filters
                user.filters.isps?.forEach((isp: string) => {
                    filterObj.bool.should.push({
                        "match": {
                            "dest_isp.keyword": isp
                        }
                    })
                });
                // add the object to query
                requestBody.query.bool.filter.push(filterObj)
            }
            // User IP LIMITS
            if (user.filters?.ips?.length > 0) {
                filterObj = {
                    "bool": {
                        "should": [
                        ],
                        "minimum_should_match": 1
                    }
                };
                // set ip filters
                user.filters?.ips?.forEach((ip: string) => {
                    filterObj.bool.should.push({
                        "match": {
                            "dest_ip.keyword": ip
                        }
                    })
                });

                // add the object to query
                requestBody.query.bool.filter.push(filterObj)
            }
            // if a user has excluded widget or data we remove
            // the aggrigation from the query so we don't make
            // the query unnecessarily complicated
            if (user.excludedWidgets.length > 0) {
                // Deleting user excluded widgets from query ( performance efficiency )
                user.excludedWidgets.forEach(excludedWidget => {
                    if (!excludedWidget) return
                    const excludedPath = Object.values(excludedWidgetsInQueryEnum)[excludedWidget]
                    const properties = excludedPath.split('.')
                    const propertyToDelete = properties.pop();
                    let obj = requestBody;
                    for (const property of properties) {
                        if (obj.aggs.hasOwnProperty(property) && typeof obj.aggs[property] === 'object') {
                            obj = obj.aggs[property];
                        } else {
                            // Property path is invalid or property doesn't exist
                            return;
                        }
                    }
                    if (obj.aggs.hasOwnProperty(propertyToDelete)) {
                        // if the obj has the last property, we basically delete it
                        delete obj.aggs[propertyToDelete];
                    }
                })
            }
        }
        // requesting elastic with the username password set in ENV file 
        let resp = firstValueFrom(this.httpService.get(
            `https://${env.get('ELASTIC_USERNAME')}:${env.get('ELASTIC_PASSWORD')}@${env.get('ELASTIC_HOST')}/log_pps_rule-detector*/_search`,
            {
                data: requestBody,
                httpsAgent: new https.Agent({
                    rejectUnauthorized: false
                })
            }
        ))
        return new Promise((resolve, reject) => {
            resp.then(async (dt) => {
                // if (!key.includes('general'))
                //     this.sendDiscordLog(key, user, requestBody, filters, Date.now() - initTime)
                let resp = dt.data

                let countriesTotal = this.getTotalFromAggs(resp.aggregations['countries'])

                let dataObj: any = {
                    countries: {
                        total: countriesTotal,
                        data: resp.aggregations['countries']['buckets'].map(r => { return { "count": r.doc_count, "key": r.key, percentage: countriesTotal ? (r.doc_count / countriesTotal * 100) : 0 } }),
                    },
                    filters: filters,
                }
                // if the data type is application visibility
                if (type == 'av') {
                    // we clean up and prepare fields for application visibility type
                    let destIPsTotal = this.getTotalFromAggs(resp.aggregations.dest_ips) + resp.aggregations?.dest_ips?.sum_other_doc_count
                    let srcPortsTotal = this.getTotalFromAggs(resp.aggregations.src_ports) + resp.aggregations?.src_ports?.sum_other_doc_count
                    const servicesPorts = JSON.parse(require('fs').readFileSync(__dirname + '/../data/servicesPorts.json'))
                    dataObj = {
                        ...dataObj,
                        ...{
                            topServices: {
                                total: srcPortsTotal,
                                data: resp.aggregations.src_ports?.buckets
                                    .filter((p) => servicesPorts.find((s) => s.port.toString() == p.key.toString()))
                                    .map(r => {
                                        let foundService = servicesPorts.find(s => s.port.toString() == r.key.toString());
                                        return {
                                            "count": r.doc_count,
                                            "key": foundService.name,
                                            percentage: srcPortsTotal ? (r.doc_count / srcPortsTotal * 100) : 0
                                        }
                                    })
                                    .splice(0, 5),
                            },
                            topGames: {
                                total: srcPortsTotal,
                                data: await this.calculateTopGames(resp.aggregations.src_ports?.buckets, srcPortsTotal),
                            },
                            topDomains: {
                                total: destIPsTotal,
                                data: await this.calculateTopDomains(resp.aggregations.dest_ips?.buckets, destIPsTotal)
                            },
                            topDestIPs: {
                                total: destIPsTotal,
                                data: [
                                    ...resp.aggregations.dest_ips?.buckets.map(r => { return { "count": r.doc_count, "key": r.key, percentage: destIPsTotal ? (r.doc_count / destIPsTotal * 100) : 0 } }).splice(0, 50),
                                    ...[{ key: 'Others', count: resp.aggregations.dest_ips?.sum_other_doc_count, percentage: resp.aggregations.dest_ips?.sum_other_doc_count / destIPsTotal * 100 }]
                                ]
                            }
                        }
                    }
                }
                // if the data type is network visibility
                if (type == 'nv') {
                    // we clean up and prepare fields for network visibility type
                    let protocolsTotal = this.getTotalFromAggs(resp.aggregations.protocols) + resp.aggregations.protocols?.sum_other_doc_count

                    let srcISPsTotal = this.getTotalFromAggs(resp.aggregations.src_isps) + resp.aggregations.src_isps?.sum_other_doc_count
                    let destISPsTotal = this.getTotalFromAggs(resp.aggregations.dest_isps) + resp.aggregations.dest_isps?.sum_other_doc_count
                    let srcASesTotal = this.getTotalFromAggs(resp.aggregations.src_ases) + resp.aggregations.src_ases?.sum_other_doc_count
                    let destASesTotal = this.getTotalFromAggs(resp.aggregations.dest_ases) + resp.aggregations.dest_ases?.sum_other_doc_count
                    let zonesTotal = this.getTotalFromAggs(resp.aggregations.topZones) + resp.aggregations.topZones?.sum_other_doc_count


                    let totalTCPsrcIPs = this.getTotalFromAggs(resp.aggregations.tcp?.src_ips) + resp.aggregations?.tcp?.src_ips?.sum_other_doc_count
                    let totalTCPdestIPs = this.getTotalFromAggs(resp.aggregations.tcp?.dest_ips) + resp.aggregations?.tcp?.dest_ips?.sum_other_doc_count
                    let totalTCPsrcPorts = this.getTotalFromAggs(resp.aggregations.tcp?.src_ports) + resp.aggregations?.tcp?.src_ports?.sum_other_doc_count
                    let totalTCPdestPorts = this.getTotalFromAggs(resp.aggregations.tcp?.dest_ports) + resp.aggregations?.tcp?.dest_ports?.sum_other_doc_count
                    let totalTCPflags = this.getTotalFromAggs(resp.aggregations.tcp?.flagsHolder?.flags) + resp.aggregations.tcp?.flagsHolder?.flags?.sum_other_doc_count

                    let totalUDPsrcIPs = this.getTotalFromAggs(resp.aggregations.udp?.src_ips) + resp.aggregations.udp?.src_ips?.sum_other_doc_count
                    let totalUDPdestIPs = this.getTotalFromAggs(resp.aggregations.udp?.dest_ips) + resp.aggregations.udp?.dest_ips?.sum_other_doc_count
                    let totalUDPsrcPorts = this.getTotalFromAggs(resp.aggregations.udp?.src_ports) + resp.aggregations.udp?.src_ports?.sum_other_doc_count
                    let totalUDPdestPorts = this.getTotalFromAggs(resp.aggregations.udp?.dest_ports) + resp.aggregations.udp?.dest_ports?.sum_other_doc_count

                    let totalICMPsrcIPs = this.getTotalFromAggs(resp.aggregations.icmp?.src_ips) + resp.aggregations.icmp?.src_ips?.sum_other_doc_count
                    let totalICMPdestIPs = this.getTotalFromAggs(resp.aggregations.icmp?.dest_ips) + resp.aggregations.icmp?.dest_ips?.sum_other_doc_count

                    // fix tcp flags where has symbols inside them like @ % *# etc.
                    resp.aggregations.tcp.flagsHolder.flags.buckets = resp.aggregations.tcp?.flagsHolder?.flags?.buckets?.filter((filter: any) => ['URG', 'ACK', 'PSH', 'RST', 'SYN', 'FIN'].some(v => filter.key.includes(v)))
                    // formatting the data
                    dataObj = {
                        ...dataObj,
                        ...{
                            topZones: this.cleanElasticResponse(zonesTotal, resp.aggregations.topZones),
                            topProtocols: this.cleanElasticResponse(protocolsTotal, resp.aggregations.protocols),
                            topSrcISPs: this.cleanElasticResponse(srcISPsTotal, resp.aggregations.src_isps),
                            topDestISPs: this.cleanElasticResponse(destISPsTotal, resp.aggregations.dest_isps),
                            topSrcASes: this.cleanElasticResponse(srcASesTotal, resp.aggregations.src_ases),
                            topDestASes: this.cleanElasticResponse(destASesTotal, resp.aggregations.dest_ases),
                            tcp: {
                                srcIPs: this.cleanElasticResponse(totalTCPsrcIPs, resp.aggregations.tcp?.src_ips),
                                destIPs: this.cleanElasticResponse(totalTCPdestIPs, resp.aggregations.tcp?.dest_ips),
                                srcPorts: this.cleanElasticResponse(totalTCPsrcPorts, resp.aggregations.tcp?.src_ports),
                                destPorts: this.cleanElasticResponse(totalTCPdestPorts, resp.aggregations.tcp?.dest_ports),
                                flags: this.cleanElasticResponse(totalTCPflags, resp.aggregations.tcp?.flagsHolder?.flags),
                                packetSize: {
                                    total: resp.aggregations.tcp?.['60t128']?.doc_count + resp.aggregations.tcp?.['128t256']?.doc_count + resp.aggregations.tcp?.['256t512']?.doc_count + resp.aggregations.tcp?.['512t1024']?.doc_count + resp.aggregations.tcp?.['gt1024']?.doc_count,
                                    "60t128": resp.aggregations.tcp?.['60t128']?.doc_count,
                                    "128t256": resp.aggregations.tcp?.['128t256']?.doc_count,
                                    "256t512": resp.aggregations.tcp?.['256t512']?.doc_count,
                                    "512t1024": resp.aggregations.tcp?.['512t1024']?.doc_count,
                                    "gt1024": resp.aggregations.tcp?.['gt1024']?.doc_count,
                                }
                            },
                            udp: {
                                srcIPs: this.cleanElasticResponse(totalUDPsrcIPs, resp.aggregations.udp?.src_ips),
                                destIPs: this.cleanElasticResponse(totalUDPdestIPs, resp.aggregations.udp?.dest_ips),
                                srcPorts: this.cleanElasticResponse(totalUDPsrcPorts, resp.aggregations.udp?.src_ports),
                                destPorts: this.cleanElasticResponse(totalUDPdestPorts, resp.aggregations.udp?.dest_ports),
                                packetSize: {
                                    total: resp.aggregations.udp?.['60t128']?.doc_count + resp.aggregations.udp?.['128t256']?.doc_count + resp.aggregations.udp?.['256t512']?.doc_count + resp.aggregations.udp?.['512t1024']?.doc_count + resp.aggregations.udp?.['gt1024']?.doc_count,
                                    "60t128": resp.aggregations.udp?.['60t128']?.doc_count,
                                    "128t256": resp.aggregations.udp?.['128t256']?.doc_count,
                                    "256t512": resp.aggregations.udp?.['256t512']?.doc_count,
                                    "512t1024": resp.aggregations.udp?.['512t1024']?.doc_count,
                                    "gt1024": resp.aggregations.udp?.['gt1024']?.doc_count,
                                }
                            },
                            icmp: {
                                srcIPs: this.cleanElasticResponse(totalICMPsrcIPs, resp.aggregations.icmp?.src_ips),
                                destIPs: this.cleanElasticResponse(totalICMPdestIPs, resp.aggregations.icmp?.dest_ips),
                                packetSize: {
                                    total: resp.aggregations.icmp?.['60t128']?.doc_count + resp.aggregations.icmp?.['128t256']?.doc_count + resp.aggregations.icmp?.['256t512']?.doc_count + resp.aggregations.icmp?.['512t1024']?.doc_count + resp.aggregations.icmp?.['gt1024']?.doc_count,
                                    "60t128": resp.aggregations.icmp?.['60t128']?.doc_count,
                                    "128t256": resp.aggregations.icmp?.['128t256']?.doc_count,
                                    "256t512": resp.aggregations.icmp?.['256t512']?.doc_count,
                                    "512t1024": resp.aggregations.icmp?.['512t1024']?.doc_count,
                                    "gt1024": resp.aggregations.icmp?.['gt1024']?.doc_count,
                                }
                            },
                        }
                    }
                }
                // setting it to mapData object
                await this.mapData.update({ [key]: dataObj })
                // and resolving the promise ( this is for when the function is called directly )
                resolve(dataObj)
            }).catch((err) => {
                console.log(err)
            })
        })
    }
    cleanElasticResponse = (total: number, respObj: any) => {
        // here we clean the elastic response
        // to our own proper response format
        if (!respObj) return {}
        return {
            total: total,
            data: [
                ...respObj?.buckets.map(r => { return { "count": r.doc_count, "key": r.key, percentage: total ? (r.doc_count / total * 100) : 0 } }).splice(0, 5),
                ...[{ key: 'Others', count: respObj?.sum_other_doc_count, percentage: respObj?.sum_other_doc_count / total * 100 }]
            ],
        }
    }

    getTotalFromAggs(resp): number {
        // will calculate total value from given
        // aggrigations object from elastic response 
        return resp?.buckets?.length > 0
            ? resp?.buckets.map(e => e.doc_count).reduce((prevVal, num) => {
                return prevVal += num
            })
            : 0
    }
    async calculateTopDomains(resp: any[], total: number) {
        return new Promise((resolve, reject) => {
            let domains: {
                [key: string]: {
                    hits: number,
                    count: number,
                    key: string,
                    percentage: number,
                    ips: number[],
                }
            } = {};
            // poz 
            const websitesDomains: { name: string, ips: string[] }[] = JSON.parse(require('fs').readFileSync(__dirname + '/../data/websitesDomains.json'))
            resp?.forEach((r, k) => {
                let foundDomains = websitesDomains.filter(s => s?.ips.includes(r.key));
                foundDomains.forEach(foundDomain => {
                    if (domains[foundDomain.name]) {
                        domains[foundDomain.name].hits += 1
                        domains[foundDomain.name].percentage += (r.doc_count / total * 100)
                        domains[foundDomain.name].count += r.doc_count
                        domains[foundDomain.name]?.ips.push(r.key)
                    } else {
                        domains[foundDomain.name] = {
                            hits: 1,
                            "count": r.doc_count,
                            "key": foundDomain.name,
                            percentage: total ? (r.doc_count / total * 100) : 0,
                            ips: [r.key],
                        }
                    }
                })
            })
            resolve(Object.values(domains).sort((a, b) => b.count - a.count).splice(0, 5))
        })
    }
    isInRange = (port: string, range: string) => {
        // this is not working quite so well, we will update it in the core
        // i just let it be here in case if we need it later
        return range.split(',').find(p => {
            return p.includes('-') ? parseInt(p.split('-')[0]) <= parseInt(port) && parseInt(port) <= parseInt(p.split('-')[1]) : parseInt(p) == parseInt(port)
        });
    }
    async calculateTopGames(resp: any[], total: number) {
        return new Promise((resolve, reject) => {
            let games: {
                [key: string]: {
                    hits: number,
                    count: number,
                    key: string,
                    percentage: number,
                    ports: number[],
                }
            } = {};
            const gamesPorts: any[] = JSON.parse(require('fs').readFileSync(__dirname + '/../data/gamesPorts.json'))
            const servicesPorts: any[] = JSON.parse(require('fs').readFileSync(__dirname + '/../data/servicesPorts.json'))
            let gamePorts = resp?.filter((r) => !servicesPorts.find(s => s.port == r.key))
            gamePorts?.forEach((r, k) => {
                let foundGames = [gamesPorts.find(s => this.isInRange(r.key, s.port))];
                foundGames.forEach(foundGame => {
                    if (games[foundGame.name]) {
                        games[foundGame.name].hits += 1
                        games[foundGame.name].percentage += (r.doc_count / total * 100)
                        games[foundGame.name].count += r.doc_count
                        games[foundGame.name].ports.push(r.key)
                    } else {
                        games[foundGame.name] = {
                            hits: 1,
                            "count": r.doc_count,
                            "key": foundGame.name,
                            percentage: total ? (r.doc_count / total * 100) : 0,
                            ports: [r.key],
                        }
                    }
                })
            })
            resolve(Object.values(games).sort((a, b) => b.count - a.count).splice(0, 5))
        })
    }
    async sendDiscordLog(key, user, query, filters, time) {

        const formData = new FormData();
        formData.append('file', Buffer.from(JSON.stringify(query, null, 1)), { filename: 'query.json' });
        formData.append('payload_json', JSON.stringify({
            content: `Query for ***${key}*** ${(user) ? `(${user?.username})` : ''}`,
            embeds: [{
                "title": (await this.dataTypes.get(key)) == 'nv' ? 'Network Visibility' : 'Application Visibility',
                "description": ``,
                "color": 16711680,
                "fields": [
                    user ? {
                        "name": "User",
                        "value": user?.username,
                        "inline": false
                    } : { "name": "", "value": "", "inline": false },
                    {
                        "name": 'Query Time',
                        "value": `${time}ms`,
                        "inline": false,
                    },
                    {
                        "name": 'filters',
                        "value": JSON.stringify(filters, null, 1),
                        "inline": false,
                    },
                ],
                "thumbnail": {
                    "url": "https://cdn.discordapp.com/attachments/1080578060954370100/1104702578182340618/siwan.png"
                }
            }]
        }));


        await this.httpService.post('https://discord.com/api/webhooks/1086932822238969887/-ATmCvltAWQ8ILe9jqPHGGrD4GvRPaLmW-FrsEDoZitot9bCqhw2_Eg3wjFp5GB8gUKa', formData)
            .toPromise()
            .then((dt) => {
            }).catch((e) => { console.log(e.response); console.log('err'); });
    }
} 