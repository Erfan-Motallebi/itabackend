import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { CacheModule } from '@nestjs/cache-manager';
import { MongooseModule } from '@nestjs/mongoose';

import * as redisStore from 'cache-manager-redis-store';

import { Log, LogSchema } from 'src/schemas/log.schema';

import { ITAController } from './ita.controller';
import { ITAService } from './ita.service';

@Module({
  imports: [
    HttpModule,
    // CacheModule.register({
    //   store: redisStore,
    //   isGlobal: true
    // }),
    MongooseModule.forFeature([
      { name: Log.name, schema: LogSchema },
    ]),
  ],
  controllers: [ITAController],
  providers: [ITAService],
  exports: [ITAService],
})
export class ITAModule { }
