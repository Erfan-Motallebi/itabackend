import { Controller, Get, Post, Body, Param, Delete, UseGuards, Put, BadRequestException, Query, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

import { JwtAuthGuard } from '@tayeh/auth';

import { AccessRoles, HasAccess, HasAccessGuard } from 'src/internal-auth/hasAccess';

import mongoose from 'mongoose';

import { diskStorage } from 'multer';

import { ITAService } from './ita.service';

@Controller(['ita'])
export class ITAController {
  constructor(private readonly itaService: ITAService) { }

  @Get('asList')
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  @UseGuards(JwtAuthGuard)
  getASList() {
    return this.itaService.getASList();
  }

  @Get('ispList')
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  @UseGuards(JwtAuthGuard)
  getDestISPList() {
    return this.itaService.getDestISPList();
  }


  @Get('liveData/:username/:type')
  getLiveData(@Param() params, @Param('type') type: 'nv' | 'av' = 'nv', @Param('username') username: string = '',) {
    if (!username) throw new BadRequestException('Wrong Username requested', null)
    if (type != 'av' && type != 'nv') throw new BadRequestException('Wrong type requested', null)
    return this.itaService.getDataFromElasticReq(type, username);
  }

}
