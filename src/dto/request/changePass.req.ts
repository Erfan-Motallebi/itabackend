import { IsNotEmpty, IsString } from 'class-validator';

export class ChangePassRequest {
    @IsString()
    @IsNotEmpty()
    currentPassword: string;

    @IsString()
    @IsNotEmpty()
    newPassword: string;

    @IsString()
    @IsNotEmpty()
    newPasswordConfirm: string;
}
