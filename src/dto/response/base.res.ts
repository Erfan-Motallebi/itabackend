export class BaseResponse<T> {
  constructor(
    public readonly status: boolean,
    public readonly message: string,
    public readonly result: T,
  ) {}
}
