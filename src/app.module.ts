import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { ServeStaticModule } from '@nestjs/serve-static/dist/serve-static.module';
import { CacheModule } from '@nestjs/cache-manager';

import { AuthModule } from '@tayeh/auth';

import { redisStore } from 'cache-manager-redis-store';

import { readFileSync } from 'fs';

import { join } from 'path';

import { AppService } from './app.service';

import { envModule } from './_shared/env';
import { InternalAuthModule } from './internal-auth/internal-auth.module';
import { ITAModule } from './ita/ita.module';
import { NotificationModule } from './notification/notification.module';
import { UserControlModule } from './user-control/user-control.module';

@Module({
  imports: [
    envModule,
    CacheModule.registerAsync<any>({
      isGlobal: true,
      useFactory: async () => {
        const store = await redisStore({
          socket: { host: 'localhost', port: 6379 },
          // ttl: 60,
        });
        return {
          store: () => store,
        };
      },
    }),
    MongooseModule.forRoot('mongodb://127.0.0.1/ita'),
    AuthModule.register({
      expire: '900s',
      private: readFileSync(__dirname + '/../.env/private.key').toString(),
      public: readFileSync(__dirname + '/../.env/public.pem').toString(),
    }),
    InternalAuthModule,
    ScheduleModule.forRoot(),
    NotificationModule,
    UserControlModule,
    ITAModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, 'media/avatars'),
      serveRoot: '/avatars/'
    }),
  ],
  providers: [AppService],
})
export class AppModule { }
