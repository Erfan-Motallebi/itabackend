import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import mongoose, { Model } from 'mongoose';

import { User, UserDocument } from '../schemas/user.schema';
import { BaseResponse } from '../dto/response';

import * as bcrypt from 'bcryptjs';

import { NotificationGateway } from '../notification/notification.gateway';
import { Log, LogDocument } from '../schemas/log.schema';

import { UserControlDto } from './dto/user-control.dto';

@Injectable()
export class UserControlService {
  constructor(
    @InjectModel(User.name) private readonly user: Model<UserDocument>,
    @InjectModel(Log.name) private readonly logs: Model<LogDocument>,
    @Inject(NotificationGateway) private notification: NotificationGateway,
  ) { }

  async create(requestedUser: UserControlDto, image?: any) {
    if (requestedUser.password) {
      requestedUser.password = await bcrypt.hash(requestedUser.password, 10)
      requestedUser.changePass = true
    }
    if (image) {
      require('fs').renameSync(image.path, `media/avatars/${requestedUser.username.trim()}.jpg`)
      requestedUser.image = `avatars/${requestedUser.username}.jpg`
    } else if (requestedUser.image == null) {
      if (require('fs').existsSync(`media/avatars/${requestedUser.username}.jpg`))
        require('fs').unlinkSync(`media/avatars/${requestedUser.username}.jpg`)
      requestedUser.image = ''
    } else {
      if (require('fs').existsSync(`media/avatars/${requestedUser.username}.jpg`))
        requestedUser.image = `media/avatars/${requestedUser.username}.jpg`
      else
        requestedUser.image = ''
    }
    if (await this.user.find({ username: requestedUser.username }).count() < 1)
      return this.user.create(requestedUser);
    else
      throw new BadRequestException(false, `There's a user with this username`)
  }

  async findAllUsers() {
    const allUsers = await this.user.find({}, {password: 0})
    return new BaseResponse(true, null, {
      allUsers
    });
  }
  async findAll(query?: object, limit = 5, page = 1) {
    const filteredRulesCount = await this.user.find(query).lean().count().exec();
    const res = await this.user.find(
      query,
      { 'updatedAt': 0, 'createdAt': 0, '__v': 0, position: 0, password: 0, changePass: 0, excludedWidgets: 0 },
      { limit: limit, skip: (page - 1) * limit },
    )
      .lean().exec();
    return new BaseResponse(true, null, {
      result: res,
      pagination: {
        total: filteredRulesCount,
        page: parseInt(page.toString()),
        limit: parseInt(limit.toString()),
      },
    });
  }

  findOne(id: mongoose.Types.ObjectId) {
    return this.user.findOne({ _id: id }, { 'updatedAt': 0, 'createdAt': 0, '__v': 0, 'password': 0 });
  }

  async update(id: mongoose.Types.ObjectId, requestedUser: UserControlDto, image?: any) {
    if (requestedUser.password) {
      requestedUser.password = await bcrypt.hash(requestedUser.password, 10)
      requestedUser.changePass = true
    }
    if (image) {
      // image added
      require('fs').renameSync(image.path, `media/avatars/${requestedUser.username.trim()}.jpg`)
      requestedUser.image = `avatars/${requestedUser.username}.jpg`
    } else if (requestedUser.image == null) {
      // image deleted
      if (require('fs').existsSync(`media/avatars/${requestedUser.username}.jpg`))
        require('fs').unlinkSync(`media/avatars/${requestedUser.username}.jpg`)
      requestedUser.image = ''
    } else {
      // not changing user
      if (require('fs').existsSync(`media/avatars/${requestedUser.username}.jpg`))
        requestedUser.image = `media/avatars/${requestedUser.username}.jpg`
      else
        requestedUser.image = ''
    }
    let resp = await this.user.updateOne({ _id: id }, requestedUser)
    this.notification.updateUserIfConnected(id)
    return resp;
  }

  remove(id: mongoose.Types.ObjectId) {
    return this.user.deleteOne({ _id: id })
  }

  //  Removing the user matching the given IDs
  //  [DELETE] http://localhost:3060/api/users/?ids[]={ID}
  async bulkRemove(ids: mongoose.Types.ObjectId[]) {
    return new BaseResponse(
      true,
      null,
      await this.user.deleteOne({
        _id: { $in: ids },
      }).exec(),
    );
  }

  async fetchLogs(query?: object, limit = 5, page = 1) {
    const filteredRulesCount = await this.logs.find(query).lean().count().exec();
    const res = await this.logs.find(
      query,
      {},
      { limit: limit, skip: (page - 1) * limit },
    )
      .sort({ 'createdAt': -1 }).lean().exec();
    return new BaseResponse(true, null, {
      result: res,
      pagination: {
        total: filteredRulesCount,
        page: parseInt(page.toString()),
        limit: parseInt(limit.toString()),
      },
    });
  }
}
