import { Test } from "@nestjs/testing";
import { getModelToken } from "@nestjs/mongoose";
import { MongoMemoryServer } from 'mongodb-memory-server'
import mongoose, { type Connection, connect, type Model } from 'mongoose'
import { Socket, io } from 'socket.io-client'
import request from 'supertest';
import { INestApplication } from "@nestjs/common";
import { sleep } from 'radash'

import { User, UserDocument, UserSchema } from "../schemas/user.schema";
import { Log, LogSchema } from "../schemas/log.schema";
import { NotificationGateway } from "../notification/notification.gateway";
import { UserControlController } from "./user-control.controller";
import { UserControlService } from "./user-control.service";
import { AccessRoles } from "src/internal-auth/hasAccess";


describe('UsersController', () => {
    const URL_PATH: string = 'http://172.26.138.178:3060';
    let app: INestApplication
    let userController: UserControlController;
    let userService: UserControlService
    let socket: Socket;

    // ! Mongo Connection Variables - Virtual Mongodb Server
    let mongod: MongoMemoryServer
    let mongoConnection: Connection
    let userModel: Model<User>
    let logModel: Model<Log>
    let token: string = "";

    beforeAll(async () => {

        mongod = await MongoMemoryServer.create()
        // const mongoURI = mongod.getUri('localhost:27017')
        const mongoURI = "mongodb://localhost:27017/ita"
        mongoConnection = (await connect(mongoURI)).connection
        userModel = mongoConnection.model(User.name, UserSchema)
        logModel = mongoConnection.model(Log.name, LogSchema)

        const moduleRef = await Test.createTestingModule({
            controllers: [UserControlController],
            providers: [UserControlService, { provide: getModelToken(Log.name), useValue: logModel }, { provide: getModelToken(User.name), useValue: userModel }, { provide: NotificationGateway, useValue: "NotificationGateway" }],
        }).compile()
        userService = moduleRef.get<UserControlService>(UserControlService)
        userController = moduleRef.get<UserControlController>(UserControlController)
        app = moduleRef.createNestApplication()
    })

    // afterAll(async () => {
    //     await mongoConnection.dropDatabase()
    //     await mongoConnection.close()
    //     await mongod.stop()
    // })

    it('should be defined', () => {
        expect(userModel).toBeDefined();
        expect(logModel).toBeDefined();
        expect(userService).toBeDefined();
        expect(userController).toBeDefined();
    });


    //   describe('Usr Model Test', () => { 

    //         it('should create a user',async () => {
    //             const allUsers = await userModel.insertMany([
    //                 {
    //                     username: "alexMCI",
    //                     password: "tester",
    //                     filters: {
    //                         isps: ["MCI"]
    //                     },
    //                     first_name: "alex",
    //                     last_name: "Alexie",
    //                     position: "TESTER"
    //                 }
    //             ], {rawResult: true})
    //             expect(allUsers).toMatchObject({insertedCount: 1})
    //         })
    //         it('should get all users',async () => {
    //             const allUsers = await userModel.find({})
    //             expect(allUsers).toBeDefined()
    //         })
    //    })

    describe('User Activities', () => {
        let users: User[] = []
        let usersWithToken: { token: string, id: string, roles: AccessRoles[], user: User }[] = []

        it('should get all users', async () => {
            const { result: { allUsers } } = await userService.findAllUsers()
            const [ita, ...allUsersExceptAdmin] = allUsers
            users = allUsersExceptAdmin
        });

        it('should login', async () => {
            for (const user of users) {
                let { body, error } = await request(`${URL_PATH}`).post("/auth/login").send({ username: user.username, password: 'tester' })
                if (error) {
                    console.error("Failed to get a user : ", user)
                }
                usersWithToken.push(body)
            }

        });

        it('should send request using sockets', async () => {
            for (const userWithToken of usersWithToken) {
                socket = io("ws://172.26.138.178:3009", { auth: { token: userWithToken.token }, transports: ["websocket"], path: "/ita" })
                socket.emit("getLiveData", 'nv')
                await sleep(1000)
            }
        },  30000);
    })
})