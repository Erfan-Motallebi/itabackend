import { Controller, Get, Post, Body, Param, Delete, UseGuards, Put, BadRequestException, Query, UploadedFile, UseInterceptors, ParseFilePipe, FileTypeValidator, MaxFileSizeValidator } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express/multer';

import { JwtAuthGuard } from '@tayeh/auth';

import { AccessRoles, HasAccess, HasAccessGuard } from './../internal-auth/hasAccess';

import mongoose from 'mongoose';

import { diskStorage } from 'multer';

import { UserControlService } from './user-control.service';

import { UserControlDto } from './dto/user-control.dto';
@Controller(['/api/users'])
@UseGuards(JwtAuthGuard)
export class UserControlController {
  constructor(private readonly userControlService: UserControlService) { }

  @Post()
  @UseGuards(HasAccessGuard)
  @HasAccess(AccessRoles.UserControl)
  @UseInterceptors(FileInterceptor('image', {
    storage: diskStorage({
      destination: 'media/avatars/',
    })
  }))
  create(@Body() userDto: UserControlDto, @UploadedFile(
    new ParseFilePipe({
      fileIsRequired: false,
      validators: [
        new MaxFileSizeValidator({ maxSize: 2e+6 }),
        new FileTypeValidator({ fileType: 'image/jpeg' }),
      ],
    }),
  ) image?: any) {
    try {
      return this.userControlService.create(userDto, image);
    } catch (error) {
      return new BadRequestException(false, error.message)
    }
  }

  @Get()
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  findAll(@Query('limit') limit?: number, @Query('page') page?: number, @Query('query') query?: object) {
    return this.userControlService.findAll(query, limit, page);
  }

  @Get(':id')
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  findOne(@Param('id') id: mongoose.Types.ObjectId) {
    return this.userControlService.findOne(id);
  }

  @Put(':id')
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  @UseInterceptors(FileInterceptor('image', {
    storage: diskStorage({
      destination: 'media/avatars/',
    })
  }))
  update(@Param('id') id: mongoose.Types.ObjectId, @Body() userDto: UserControlDto, @UploadedFile(
    new ParseFilePipe({
      fileIsRequired: false,
      validators: [
        new FileTypeValidator({ fileType: 'image/jpeg' }),
      ],
    }),
  ) image?: any) {
    return this.userControlService.update(id, userDto, image);
  }

  @Delete(':id')
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  remove(@Param('id') id: mongoose.Types.ObjectId) {
    return this.userControlService.remove(id);
  }

  //  Removing the user matching the given IDs
  //  [DELETE] http://localhost:3060/api/users/?ids[]={ID}
  @Delete('')
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  removeBulk(@Query('ids') ids: mongoose.Types.ObjectId[]) {
    return this.userControlService.bulkRemove(ids);
  }

  @Get('/logs/fetch')
  @HasAccess(AccessRoles.UserControl)
  @UseGuards(HasAccessGuard)
  getLogs(@Query('limit') limit?: number, @Query('page') page?: number, @Query('query') query?: object) {
    return this.userControlService.fetchLogs(query, limit, page);
  }

}
