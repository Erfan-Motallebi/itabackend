import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { User, UserSchema } from 'src/schemas/user.schema';
import { NotificationModule } from 'src/notification/notification.module';
import { Log, LogSchema } from 'src/schemas/log.schema';

import { UserControlController } from './user-control.controller';
import { UserControlService } from './user-control.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Log.name, schema: LogSchema },
    ]),
    NotificationModule
  ],
  controllers: [UserControlController],
  providers: [UserControlService]
})
export class UserControlModule { }
