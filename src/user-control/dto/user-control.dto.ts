import { Prop } from "@nestjs/mongoose";

import { IsBoolean, IsNotEmpty, IsOptional, IsString } from "class-validator";

import { AccessRoles, excludedWidgetsEnum } from "../../internal-auth/hasAccess";

export class Filter {
    @Prop()
    @IsString()
    ases: Number[];

    @Prop()
    @IsString()
    isps: String[];

    @Prop()
    @IsString()
    ips: String[];
}
export class UserControlDto {
    @Prop()
    @IsNotEmpty()
    @IsString()
    username: string;

    @Prop()
    @IsOptional()
    @IsString()
    password?: string;

    @Prop({ type: [String], enum: AccessRoles, default: AccessRoles.General })
    roles: AccessRoles[];

    @Prop({ type: Filter, default: { ases: [], ips: [], isps: [] } })
    filters: Filter;

    @Prop({ type: [Number], enum: excludedWidgetsEnum })
    excludedWidgets: excludedWidgetsEnum[];

    @Prop()
    @IsOptional()
    @IsString()
    first_name: string;

    @Prop()
    @IsOptional()
    @IsString()
    last_name: string;

    @Prop()
    @IsOptional()
    @IsString()
    position: string;

    @Prop()
    @IsOptional()
    @IsString()
    image?: string;

    @Prop()
    @IsOptional()
    @IsString()
    mobile?: string;

    @Prop({ default: true })
    @IsBoolean()
    @IsOptional()
    status: boolean;

    @Prop()
    @IsBoolean()
    @IsOptional()
    changePass?: boolean;
}
