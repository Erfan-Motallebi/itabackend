import { Inject, CACHE_MANAGER, forwardRef } from '@nestjs/common';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { InjectModel } from '@nestjs/mongoose';

import { EnvService } from '@tayeh/env';

import { Cache } from 'cache-manager';

import { Socket, Server } from 'socket.io';

import { decode } from 'jsonwebtoken'

import mongoose, { Model } from 'mongoose';

import { User, UserDocument } from '../schemas/user.schema';
import { ITAService } from '../ita/ita.service';
import { AsyncRedisProperty } from '../_shared/env';

import redisIoAdapter from 'socket.io-redis';

import { Log, LogDocument } from '../schemas/log.schema';

const env = new EnvService('./.env/development.env');

@WebSocketGateway(parseInt(env.get('WS_PORT')) || 3009, {
  cors: {
    origin: '*',
  },
  transports: ['websocket'],
  // @ts-ignore
  adapter: redisIoAdapter({ port: 6379, host: 'localhost' }),
  httpCompression: true,
  path: '/ita',
})
// @UseGuards(JwtAuthGuard)
export class NotificationGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    @Inject(forwardRef(() => ITAService)) private itaService: ITAService,
    @InjectModel(User.name) private readonly user: Model<UserDocument>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    @InjectModel(Log.name) private readonly logger: Model<LogDocument>,
  ) { }
  @WebSocketServer() server: Server;
  socketId: string
  private users = new AsyncRedisProperty<any>({
    cacheManager: this.cacheManager,
    key: 'itaUsersAndSockets',
    default: {},
  });

  afterInit(server: Server) {
    console.log(`[SOCKET] Notification Socket Initialized`);
    this.users.setDefault();
  }

  /**
   * @function getUserFromSocket
   * @summary It checks the user from socket handshake auth then
   * @param clientSocket Socket recieved from the client
   * @returns User
   */

  // TODO: Check user authenticity through the socket handshake received through the client socket id
  // NOTE: User's socket id changes periodically. You should update it every single socket connection

  async getUserFromSocket(clientSocket: Socket) {
    const authenticationToken = clientSocket.handshake.auth?.token;
    const user =
      authenticationToken ?
        (await this.user
          .findOne({ _id: decode(authenticationToken)?.sub })
          .lean()
          .exec())
        : await this.users.get(clientSocket?.id);
    if (user) {
      if (!(await this.users.get(clientSocket?.id)))
        this.logger.create({
          url: ``,
          module: 'GATEWAY',
          username: user?.username,
          message: `Socket Connected`,
        });
      this.users.update({ [clientSocket?.id]: user });
      //* Getting the previous socketId to handle out the logout action
      this.socketId = clientSocket?.id
    } else {
      console.log('disconnecting user');
      clientSocket.disconnect();
    }
    return user;
  }

  /**
   * @function handleConnection
   * @summary It always checks user authenticity using socket handshake every single connection
   * @param clientSocket Socket recieved from the client
   */
  async handleConnection(clientSocket: Socket, ...args: any[]) {
    console.log("Handling connection");
    await this.getUserFromSocket(clientSocket);
  }

  async handleDisconnect(clientSocket: Socket, ...args: any[]) {

    const clientSocketId = this.socketId ?? clientSocket?.id
    if (!(await this.users.get(clientSocketId))) return;
    console.log(
      `[GATEWAY] => [${(await this.users.get(clientSocket?.id))?.username
      }] => handleDisconnect => Disconnected`,
    );
    // console.log({ clientId: clientSocket.id });
    this.logger.create({
      url: ``,
      module: 'GATEWAY',
      username: (await this.users.get(clientSocketId))?.username,
      message: `Socket Disconnected`,
    });

    //! Deleting the extensive dataTypes / users in Redis while refreshing the page [ Controlling redis ]
    await this.users.del(clientSocketId);
    const dataTypes: {} = await this.cacheManager.get('dataTypes')
    if (clientSocketId in dataTypes) {
      delete dataTypes[clientSocketId]
      await this.cacheManager.set('dataTypes', dataTypes)
    }

    await this.itaService.handleUserDisconnect(clientSocket);
  }

  async getUserModel(username: string) {
    return await this.user.findOne({ username: username }).lean().exec();
  }

  async updateUserIfConnected(userID: mongoose.Types.ObjectId) {
    let user = await this.user.findOne({ _id: userID }).exec();
    console.log(
      'UpateUserIfConnected',
      Object.keys(await this.users.get()).filter(
        async (key) => (await this.users.get(key))._id == userID,
      ),
    );
    Object.keys(await this.users.get())
      .filter(async (userKey) => (await this.users?.get(userKey))._id == userID)
      .forEach((socketID) => {
        this.itaService.flushUserData(socketID);
        this.server.sockets.sockets?.get(socketID)?.emit('userUpdated', user);
      });
  }


  @SubscribeMessage('logout')
  async logoutUserHandler(clientSocket: Socket, ...args: any[]) {
    await this.handleDisconnect(clientSocket)
  }

  /**
   * @function handleSetUserFilter
   * @summary It listens to the socket named "setUserFilter"
   * @param clientSocket Socket recieved from the client
   * @param filters Filters received from the user filter panel
   * @return
   */

  // TODO: Get the users' filter panel
  // TODO: Check the users' data type [ nv / av ] through their socket id already saved in


  @SubscribeMessage('setUserFilter')
  async handleSetUserFilter(clientSocket: Socket, filters: any) {
    console.log(
      `[GATEWAY] => [${(await this.users.get(clientSocket.id))?.username
      }] => setUserFilter => ${JSON.stringify(filters)}`,
    );
    const dataType =
      this.cacheManager.get('dataTypes')[clientSocket?.id] == 'nv'
        ? 'network'
        : 'application';
    this.logger.create({
      url: `map/${dataType}`,
      module: 'MAP/SetFilter',
      username: (await this.users.get(clientSocket.id))?.username,
      message: `User setting filter for ${dataType} Visibility\nFilters: ${JSON.stringify(
        filters,
      )}`,
    });
    this.itaService.setUserFilter(clientSocket, filters);
  }

  /**
   *@function handleGetLiveData
   * @summary It listens to the the socket named "getLiveData"
   * @param clientSocket Socket recieved from the client
   * @param type data type used in order to get specific map data [ network or application ]
   * @returns
   */

  // TODO: Get user's data type to see whether they selected the network or application tab

  @SubscribeMessage('getLiveData')
  async handleGetLiveData(clientSocket: Socket, type: 'nv' | 'av' = 'nv') {
    const userInRedis = await this.users.get(clientSocket.id)
    console.log(
      `[GATEWAY] => [${userInRedis?.username
      }] => getLiveData => Requested Live Data`,
    );
    const dataType = type == 'nv' ? 'network' : 'application';
    this.logger.create({
      url: `map/${dataType}`,
      module: 'MAP/LiveData',
      username: (await this.users.get(clientSocket.id))?.username,
      message: `Requested Live ${dataType} Data`,
    });

    //! Set dataTypes then get the live data.
    this.itaService.setDataType(clientSocket, type);
  }

  async getUserFromSocketID(clientID: string): Promise<User> {
    const user = await this.users.get(clientID);
    if (user) return await this.user.findOne({ _id: user?._id }).exec();
    else return null;
  }



  @SubscribeMessage('msgToServer')
  handleMessage(client: Socket, payload: string): void {
    this.server.emit('msgToClient', payload);
  }
}
