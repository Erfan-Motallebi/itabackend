import { Global, Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CacheModule } from '@nestjs/cache-manager';

import { ITAModule } from 'src/ita/ita.module';

import * as redisStore from 'cache-manager-redis-store';

import { Log, LogSchema } from 'src/schemas/log.schema';

import { User, UserSchema } from '../schemas/user.schema';

import { NotificationGateway } from './notification.gateway';

@Global()
@Module({
    imports: [
        ITAModule,
        // CacheModule.register({
        //     store: redisStore,
        //     isGlobal: true
        // }),
        MongooseModule.forFeature([
            { name: User.name, schema: UserSchema },
            { name: Log.name, schema: LogSchema },
        ]),
    ],
    providers: [NotificationGateway],
    exports: [NotificationGateway],
})
export class NotificationModule { }
